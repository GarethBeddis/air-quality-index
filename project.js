// ------------------------------------------------------------------
// JOVO PROJECT CONFIGURATION
// ------------------------------------------------------------------

module.exports = {
    stages: {
      dev: {
        alexaSkill: {
            nlu: 'alexa',
            endpoint: 'arn:aws:lambda:eu-west-1:394873125572:function:air-quality-index',
            skillId: 'amzn1.ask.skill.d9736f8d-e079-4874-994e-1d1381d4d60f',
            lang: {
                en: [
                    'en-GB',
                ]
            },
            manifest: {
                apis: {
                    custom: {
                        interfaces: [{
                            type: 'ALEXA_PRESENTATION_APL',
                        }, ],
                    },
                },
            },
        },
        googleAction: {
            endpoint: 'https://75dvcmp9bk.execute-api.eu-west-1.amazonaws.com/dev',
            projectId: 'air-quality-index',
            keyFile: './src/credentials/service-account.json',
          },
      },
    },
  };
  