module.exports = aqi => {
    switch (true) {
      case aqi <= 50:
        return "good";
      case aqi <= 100:
        return "moderate";
      case aqi <= 150:
        return "unhealthy for sensitive groups";
      case aqi <= 200:
        return "unhealthy";
      case aqi <= 300:
        return "very unhealthy";
      case aqi <= 500:
        return "hazardous";
      default:
        return "N/A";
    }
  };