module.exports = (jovo, documentPath, datasourcePath) => {
  if (jovo.$request.context.System.device.supportedInterfaces['Alexa.Presentation.APL']) {
    jovo.$alexaSkill.addDirective({
      type: 'Alexa.Presentation.APL.RenderDocument',
      version: '1.0',
      document: documentPath, 
      datasources: datasourcePath, 
    });
  }
};
