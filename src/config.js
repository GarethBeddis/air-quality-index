module.exports = {
  logging: true,

  intentMap: {
    "AMAZON.StopIntent": "END"
  },

  db: {
    DynamoDb: {
      tableName: "userData"
    }
  },
  user: {
    metaData: {
      enabled: true
    },
    context: {
      enabled: true
    }
  },
  analytics: {
    // Configuration for generic tracking plugin
    GoogleAnalytics: {
        trackingId: 'UA-75190709-2',
        trackDirectives: true   // Optional
    },
    // Configurations for platform-specific plugins
    GoogleAnalyticsAlexa: {
        trackingId: 'UA-75190709-2'
    },
    GoogleAnalyticsGoogleAssistant: {
        trackingId: 'UA-75190709-2'
    }
}

};
