"use strict";

// ------------------------------------------------------------------
// APP INITIALIZATION
// ------------------------------------------------------------------

const { App } = require("jovo-framework");
const { Alexa } = require("jovo-platform-alexa");
const { GoogleAssistant } = require("jovo-platform-googleassistant");
const { JovoDebugger } = require("jovo-plugin-debugger");
const { FileDb } = require("jovo-db-filedb");
const rp = require("request-promise");
const { DynamoDb } = require("jovo-db-dynamodb");
const { GoogleAnalytics } = require('jovo-analytics-googleanalytics')
const mailgun = require("mailgun-js");

// Helpers
const buildAPL = require('./helpers/build-apl');
const getAqiDataSource = require('./helpers/build-aqi-data-source');
const getAqiCondition = require('./helpers/get-aqi-condition')

const app = new App();

app.use(
    new Alexa(),
    new GoogleAssistant(),
    new JovoDebugger(),
    new FileDb(),
    new DynamoDb(),
    new GoogleAnalytics
);

// ------------------------------------------------------------------
// APP LOGIC
// ------------------------------------------------------------------

app.setHandler({
    // TODO: refactor intent, flow needs cleaning up so that there's a clear next step for user following account linking
    // TODO: save user city if permissions for address are granted
    async HandleAccountLinking() {
        if (!this.$request.getAccessToken()) {
            this.$speech.addText(
                "Welcome to Air Quality Index! Please open the Amazon Alexa app to link your account to get started."
            );
            this.$alexaSkill.showAccountLinkingCard();
            this.tell(this.$speech);
        } else {
            let url = `https://api.amazon.com/user/profile?access_token=${this.$request.getAccessToken()}`;

            await rp(url).then(body => {
                let data = JSON.parse(body);

                this.$user.$data.email = data.email;
            });
        }
    },

    LAUNCH() {
        const firstTimeUser = !this.$user.$metaData.sessionsCount;

        if (firstTimeUser) {
            return this.toIntent("HandleAccountLinking");
        } else {
            this.$speech.addText(
                `Hi! I can give you information on pollution across any city in the world. What city would you like to know about?`
            );
            this.$reprompt.addText(`What city would you like to know about?`);
            this.followUpState("GetAqiIntent").ask(this.$speech, this.$reprompt);
        }
    },

    MyCityIsIntent() {
        this.$user.$data.location = this.$inputs.location.value;
        this.$speech.addText(
            `Thanks, I'll remember that you live in ${this.$user.location.value} from now on.`
        );
        this.followUpState("GetAqiIntent").tell(this.$speech);
    },

    async GetAqiIntent() {
        // GET Request to WAQI API
        const url = `https://api.waqi.info/feed/${this.$inputs.location.value}/?token=2bf89bb0ac470ebc6e7874d911227f5af604fdc2`;

        await rp(url).then(body => {
            let response = JSON.parse(body);
            if (response.status != "error") {

                const aqi = response.data.aqi;
                const indexScale = getAqiCondition(aqi);

                // Store user data
                this.$user.$data.location = this.$inputs.location.value;
                this.$user.$data.indexScale = indexScale;
                this.$user.$data.aqi = aqi;

                // Visual Response
                buildAPL(this, `require("../apl/docs/${template}.json"`, getAqiDataSource(this.$inputs.location.value, indexScale, aqi))

                // Analytics
                this.$googleAnalytics.sendUserEvent('Location', this.$inputs.location.value);

                // Create and send response
                this.$speech.addText(
                    `The Air Quality Index rating in ${this.$inputs.location.value} is currently ${indexScale} at ${aqi}. Would you like me to send more information to your email?`
                );
                this.followUpState("EmailState").ask(this.$speech);

            } else {
                // Error handle
                this.tell(`Sorry, I can't find any data for that location!`);
            }
        });
    },

    EmailState: {
        async YesIntent() {
            // TODO: Error handling, check email permissions have been granted
            let url = `https://api.amazon.com/user/profile?access_token=${this.$request.getAccessToken()}`;

            await rp(url).then(body => {
                let data = JSON.parse(body);
                this.$user.$data.email = data.email;
            });

            // Mailgun config
            const DOMAIN = "sandbox06ecef3a7e4d4451a3d082bc28a97e48.mailgun.org";
            const mg = mailgun({
                apiKey: "6752311594d2fa66b2c79402afe3c098-9a235412-df59664e",
                domain: DOMAIN
            });

            // Email data
            const data = {
                from:
                    "Mailgun Sandbox <postmaster@sandbox06ecef3a7e4d4451a3d082bc28a97e48.mailgun.org>",
                to: this.$user.$data.email,
                subject: `Request for Air Quality Index: ${this.$user.$data.location}`,
                text: `The Air Quality Index in ${this.$user.$data.location} is currently ${this.$user.$data.indexScale} at ${this.$user.$data.aqi}. https://aqicn.org/city/${this.$user.$data.location}`
            };

            // Attempt to send email
            await mg.messages().send(data, function (error, body) {
                if (error) {
                    console.debug(error);
                }
                console.log(body);
            });

            // Response
            this.followUpState(null).tell(
                `Great! I've sent more information to your email`
            );
        },

        NoIntent() {
            this.followUpState(null).tell(
                `Okay, I won't send more info to your email`
            );
        }
    }
});

module.exports.app = app;

// MyCityIsIntent model
// {
//     "name": "MyCityIsIntent",
//     "phrases": [
//         "Remember that I live in {location}",
//         "I live in {location}",
//         "My area is {location}",
//         "My city is {location}",
//         "My home is in {location}",
//         "My hometown is {location}"
//     ],
//     "inputs": [
//         {
//             "name": "location",
//             "type": {
//                 "alexa": "AMAZON.City",
//                 "dialogflow": "@sys.geo-city"
//             }
//         }
//     ]
// },
